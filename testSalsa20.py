import unittest
from utils import HTMLTestRunner
import os
import re
import optparse
import sys


from MySalsa20 import Salsa20


class TestSalsa(unittest.TestCase):
    ''' Clase de unittest para testear salsa 20
        Tiene solo funciones genericas, los tests se generan dinamicamente apartir de la info de los files
    '''

    def setUp(self):
        self.rounds = 20

    def pretty_hex_string(self, data):
        ''' Para generar un string que sea bonito para imprimir '''
        output = "\n"
        for i in range(0, len(data), 32):
            output += data[i:i+32] + '\n'
        output += "\n"
        return output

    def xor_strings(self, xs, ys):
        ''' Function para hacer xor de dos strings '''
        return "".join(chr(ord(x) ^ ord(y)) for x, y in zip(xs, ys))

    def calcular_xor_digest(self, ciphertxt):
        ''' Calculo un digest xor, es decir tomo todo el cipfrado y lo reduzco a un solo xor de 64 bytes'''
        xor_digest = ciphertxt[:64]
        for i in range(64, len(ciphertxt), 64):
            xor_digest = self.xor_strings(xor_digest, ciphertxt[i:i+64])

        return xor_digest

    def tearDown(self):
        pass


def test_generator(test_data):
    def generic_test(self):
        message = chr(0) * 512
        print '\nTesting 512 zero bytes. Salsa 20 %s rounds' % self.rounds
        key = test_data['key'].decode('hex')
        IV = test_data['IV'].decode('hex')
        print 'Key'.center(20, '=')
        print key.encode('hex').upper()
        print 'IV'.center(20, '=')
        print IV.encode('hex').upper()
        s20 = Salsa20(key, IV, self.rounds)
        ciphertxt = s20.encrypt(message)

        #Checkeo pedazos del texto cifrado
        for key, stream_expected in test_data['streams'].iteritems():
            inic, fin = key.split('..')
            idx_i, idx_f = int(inic), int(fin) + 1
            stream = ciphertxt[idx_i:idx_f].encode('hex').upper()
            self.assertEqual(stream, stream_expected, '\n' + "=" * 32 + "\nCipherText bytes [%s:%s] are not the expected \n Expected are: \
                                                      \n %s \n Found are:\n %s " % (str(idx_i), str(idx_f), self.pretty_hex_string(stream_expected),
                                                      self.pretty_hex_string(stream)))

        xor_digest = self.calcular_xor_digest(ciphertxt).encode('hex').upper()
        self.assertEqual(xor_digest, test_data['xor-digest'], "\n Bad Encrypt\n" + "Expcted Is".center(64, "=") + '\n' + self.pretty_hex_string(test_data['xor-digest']) +
                                                              "\n" + "But Found Is".center(64, "=") + '\n' + self.pretty_hex_string(xor_digest))
    return generic_test


def leer_test_vector(f):
    '''Funcion que va devolviendo los test que encuentra en el file que recibe '''
    #for line in f:
    line = "ini"
    while line:
        data = ""
        line = f.readline()
        if not line:
            continue

        while line and not line.startswith('Set'):
            data += line
            line = f.readline()
        else:
            if data:
                data = data.split('\n')
                test = {'streams': {}}
                for l in data:
                    if l:
                        key, value = l.strip().split('=')
                        key = key.strip()
                        if key.startswith('stream'):
                            pattern = re.compile("stream|\[|\]")
                            new_key = pattern.sub("", key)
                            test['streams'][new_key] = value.strip()
                        else:
                            test[key.strip()] = value.strip()
                yield test


def generar_test(file_vectors):
    f = open(file_vectors, "r")
    generator_vectors = leer_test_vector(f)
    idx = 1
    for test_data in generator_vectors:
        test_name = 'test_%s' % idx
        #test_data = generator_vectors.next()
        test = test_generator(test_data)
        setattr(TestSalsa, test_name, test)
        idx += 1
    f.close()


if __name__ == '__main__':
    parser = optparse.OptionParser()

    parser.add_option('-f', '--file-vectors', action="store", dest="file_vectors", help="File with test vectors to be run")
    options, args = parser.parse_args()
    if not options.file_vectors:
        print "You need to provide a file with test vectors"
        sys.exit()
    file_vectors = options.file_vectors

    generar_test(file_vectors)

    #Correr test y generar reporte
    suite = unittest.TestLoader().loadTestsFromTestCase(TestSalsa)
    unittest.TextTestRunner(verbosity=2).run(suite)

    outfile = open("TestResultsReport_%s.html" % os.path.basename(file_vectors), "w")
    runner = HTMLTestRunner.HTMLTestRunner(stream=outfile,
                                           title='Test Results Report %s' % file_vectors,
                                           description='Reporte de testcases para Salsa 20.')
    runner.run(suite)
