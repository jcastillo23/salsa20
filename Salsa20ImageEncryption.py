from PIL import Image
from MySalsa20 import Salsa20
import os


class ImageEncryptSalsa(object):
    '''Clase que se encarga de encriptar una imagen usando la lib de Salsa20 '''
    def __init__(self, imagePath, key, IV, salsaRounds=20, callback=lambda x: x):
        self.key = key
        self.IV = IV
        self.salsaRounds = salsaRounds
        self.img_path = os.path.abspath(imagePath)
        self._img_dir = os.path.dirname(self.img_path)
        self._img_name, self._img_extension = os.path.splitext(os.path.basename(self.img_path))

        self.callback = callback

    def _transform_image(self, image):
        """
        Funcion que se encarga de hacer tanto la encriptacion como la desencriptacion ya que el proceso es el mismo para ambos casos
        recibe un Image object(que puede estar en plano o encriptado) y devuelve un Image object encriptado o en plano segun corresponda
        """
        pixels = image.load()
        width = image.size[0]
        height = image.size[1]

        pixels_str = ""
        #Me quedo con la lista de RGBs en un string
        for y in range(0, height):
            for x in range(0, width):
                pixR, pixG, pixB = pixels[x, y][0:3]
                pixels_str += chr(pixR) + chr(pixG) + chr(pixB)

        s20 = Salsa20(self.key, self.IV, self.salsaRounds)
        ciphertxt = s20.encrypt(pixels_str, self.callback)  # Llamo siempre a encrpyt porque es lo mismo encriptar que desencriptar

        #Transformo el texto encriptado en una lista de RGB
        new_image_rgb = list()
        for i in xrange(0, len(ciphertxt), 3):
            r, g, b = ord(ciphertxt[i]), ord(ciphertxt[i+1]), ord(ciphertxt[i+2])
            new_image_rgb.append((r, g, b))

        new_image = Image.new("RGB", (int(width), int(height)))
        new_image.putdata(new_image_rgb)
        return new_image

    def encrypt(self):
        img = Image.open(self.img_path)
        encrypted = self._transform_image(img)
        new_name = os.path.join(self._img_dir, self._img_name) + "_crypt%s" % self._img_extension
        encrypted.save(new_name)
        #encrypted.show()
        return new_name

    def decrypt(self):
        img = Image.open(self.img_path)
        decrypted = self._transform_image(img)
        new_name = os.path.join(self._img_dir, self._img_name) + "_decrypt%s" % self._img_extension
        decrypted.save(new_name)
        #decrypted.show()
        return new_name


if __name__ == '__main__':
    import sys
    image = sys.argv[1]
    cifrador = ImageEncryptSalsa(image, "0053A6F94C9FF24598EB3E91E4378ADD3083D6297CCF2275C81B6EC11467BA0D".decode('hex'), "0000000000000000".decode('hex'))
    encrypted_path = cifrador.encrypt()

    descifrador = ImageEncryptSalsa(encrypted_path, "0053A6F94C9FF24598EB3E91E4378ADD3083D6297CCF2275C81B6EC11467BA0D".decode('hex'), "0000000000000000".decode('hex'))
    descifrador.decrypt()
