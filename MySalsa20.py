from array import array
from struct import Struct
little_u64 = Struct("<Q")  # little-endian 64-bit unsigned.
little16_i32 = Struct("<16I")  # 16 little-endian 32-bit signed ints.
little4_i32 = Struct("<4i")    # 4 little-endian 32-bit int
little2_i32 = Struct("<2i")    # 2 little-endian 32-bit int

#### Funciones Matematicas de ayuda ####


def add32(u, v):
    ''' Suma en de dos words en modulo 2^32'''
    return (u + v) % pow(2, 32)


def shift_l(u, c):
    ''' Shiftea hacia la izquierda el numero C de bits y lo mantiene en 32 bits'''
    return ((u << c) | (u >> (32 - c))) & 0xffffffff


class Salsa20(object):
    TAU = little4_i32.unpack("expand 16-byte k")    # constante para claves de 16-bytes se interpreta como 4 enteros de 32 bits
    SIGMA = little4_i32.unpack("expand 32-byte k")  # constante para claves de 32-bytes se interpreta como 4 enteros de 32 bits
    ROUNDS = 20   # Salsa acepta 8,12 o 20 rounds, pero el default es 20

    def __init__(self, key, iv='\x00'*8, rounds=ROUNDS):
        if rounds not in [20, 12, 8]:
            raise Exception('El numero de Rounds debe ser 8,12 o 20')
        self._setup_key(key)
        self.setup_iv(iv)
        self.lastchunk = True     # Con esto nos aseguramos que todos salvo el ultimo chunk son multplos de 64
        self.ROUNDS = rounds

    def _quarterround(self, y):
        ''' Funcion propia de salsa20 para hacer el hash de la key
            Y , Z son secuencias de 4-word elements (1 word = 32 bits)
        '''
        z = [0] * 4
        z[1] = y[1] ^ shift_l(add32(y[0], y[3]), 7)
        z[2] = y[2] ^ shift_l(add32(z[1], y[0]), 9)
        z[3] = y[3] ^ shift_l(add32(z[2], z[1]), 13)
        z[0] = y[0] ^ shift_l(add32(z[3], z[2]), 18)
        return z

    def _rowround(self, y):
        ''' Funcion propia de salsa20 para hacer el hash de la key
            Y , Z son secuencias de 16-word elements
        '''
        z = [0] * 16
        z[0],  z[1],  z[2],  z[3] = self._quarterround([y[0], y[1], y[2], y[3]])
        z[5],  z[6],  z[7],  z[4] = self._quarterround([y[5], y[6], y[7], y[4]])
        z[10], z[11], z[8],  z[9] = self._quarterround([y[10], y[11], y[8], y[9]])
        z[15], z[12], z[13], z[14] = self._quarterround([y[15], y[12], y[13], y[14]])
        return z

    def _columnround(self, x):
        ''' Funcion propia de salsa20 para hacer el hash de la key
            X , Y son secuencias de 16-word elements
        '''
        y = [0] * 16
        y[0],  y[4],  y[8],  y[12] = self._quarterround([x[0], x[4], x[8], x[12]])
        y[5],  y[9],  y[13], y[1] = self._quarterround([x[5], x[9], x[13], x[1]])
        y[10], y[14], y[2],  y[6] = self._quarterround([x[10], x[14], x[2], x[6]])
        y[15], y[3],  y[7],  y[11] = self._quarterround([x[15], x[3], x[7], x[11]])
        return y

    def _doubleround(self, x):
        ''' Funcion propia de salsa20 para hacer el hash de la key ( seria como una difusion)
            X es una secuencia de 16-word elements
        '''
        return self._rowround(self._columnround(x))

    def _setup_key(self, key):
        """
        Salsa 20 trabaja tomando una key inicial de 16 o 32 bytes y la expande a 64 bytes
        """
        if len(key) not in [16, 32]:
            raise Exception('La clave debe ser de 16 o 32 bytes')
        expanded_key = [0]*16  # Cada subindice de la expanded key es un elemento de 4 bytes por lo cual 16 * 4 = 64 bytes
        if len(key) == 16:
            #Claves de 16 bytes de longitud
            k = list(little4_i32.unpack(key))  # Interpreto la clave como 4 enteros de 32-bits cada uno
            expanded_key[0] = self.TAU[0]
            expanded_key[1] = k[0]
            expanded_key[2] = k[1]
            expanded_key[3] = k[2]
            expanded_key[4] = k[3]
            expanded_key[5] = self.TAU[1]
            #Dejamos 16 bytes para el IV (indices 6,7) y el counter (indices 8,9
            expanded_key[10] = self.TAU[2]
            expanded_key[11] = k[0]
            expanded_key[12] = k[1]
            expanded_key[13] = k[2]
            expanded_key[14] = k[3]
            expanded_key[15] = self.TAU[3]
        elif len(key) == 32:
            #Claves de 32 bytes de longitud
            k = list(little4_i32.unpack(key[0:16]))  # Interpreto la mitad de la clave como 4 enteros de 32-bits cada uno
            expanded_key[0] = self.SIGMA[0]
            expanded_key[1] = k[0]
            expanded_key[2] = k[1]
            expanded_key[3] = k[2]
            expanded_key[4] = k[3]
            expanded_key[5] = self.SIGMA[1]
            #Dejamos 16 bytes para el IV (indices 6,7) y el counter (indices 8,9)
            k = list(little4_i32.unpack(key[16:32]))  # Interpreto la segunnda mitad de la clave como 4 enteros de 32-bits cada uno
            expanded_key[10] = self.SIGMA[2]
            expanded_key[11] = k[0]
            expanded_key[12] = k[1]
            expanded_key[13] = k[2]
            expanded_key[14] = k[3]
            expanded_key[15] = self.SIGMA[3]
        self.expanded_key = expanded_key

    def setup_iv(self, iv):
        """
        Seteo en vector de inicializacion dentro del expanded_key
        """
        if len(iv) != 8:
            raise Exception('El Vector de Inicializacion debe ser de 8bytes de longitud')
        v = list(little2_i32.unpack(iv))
        self.expanded_key[6] = v[0]
        self.expanded_key[7] = v[1]
        self.expanded_key[8], self.expanded_key[9] = 0, 0  # Contador inicializado

    def _incrementCounter(self):
        '''Esta funcion esta hecha para poder tomar el contador de cada etapa como un solo elemento de 8 bytes'''
        counter = little_u64.unpack(little2_i32.pack(*self.expanded_key[8:10]))[0]
        new_counter = add32(counter, 1)  # % pow(2, 32)
        self.expanded_key[8], self.expanded_key[9] = little2_i32.unpack(little_u64.pack(new_counter))

    def encrypt(self, data, callback=lambda x: x):
        """
        La propia funcion de encripcion de Salsa20
        Primero tomca el expanded_key y lo hashea para formar un bloque de 64bytes y eso lo voy xoreando con el plain text
        para poder cifrar
        - callback : Es una funcion a ser llamada cada vez que voy leyendo 64bytes del input para notificar el porcentaje de avance
        """
        if not self.lastchunk:
            # Esto se debe a que si se llama a esta funcion con chunk a cifrar si se usa uno que no es multiplo de 64bytes
            # luego va a haber "redundancia" a la  hora de desencriptar
            raise Exception('size of last chunk not a multiple of 64 bytes')
        len_data = len(data)
        percent = float(64 * 100) / len_data
        out = array('c', '\x00' * len_data)
        for i in xrange(0,  len_data, 64):
            h = self._salsa20_hash_function()
            self._incrementCounter()
            for j in xrange(min(64, len_data - i)):
                out[i+j] = chr(ord(data[i+j]) ^ ord(h[j]))
            self._lastChunk64 = not len_data % 64
            callback(percent)
        callback(percent)
        return out.tostring()

    decrypt = encrypt  # tanto para encryptar como para desencryptar es el mismo proceso.

    def _salsa20_hash_function(self):
        """
        Toma el expanded_key ( la key expandida) y le aplica la funcion double round la mitad de los rounds definidos.
        Luego hace una suma en 32bits de el hash que le quedo con el estado inicial y con eso queda el hash final
        """
        hashed_expandedKey = self.expanded_key[:]
        for i in xrange(0, self.ROUNDS, 2):
            hashed_expandedKey = self._doubleround(hashed_expandedKey)
        for i in xrange(16):
            hashed_expandedKey[i] = add32(hashed_expandedKey[i], self.expanded_key[i])
        output = little16_i32.pack(*hashed_expandedKey)  # Lo packeo como 16 enteros de 32-bit

        return output


def quick_test():
    ''' Test Rapido '''
    rounds = 8
    f = open('testdata.txt', 'rb')
    message = f.read()
    f.close()
    key = '0053A6F94C9FF24598EB3E91E4378ADD3083D6297CCF2275C81B6EC11467BA0D'.decode('hex')
    IV = '0D74DB42A91077DE'.decode('hex')
    # encrypt
    s20 = Salsa20(key, IV, rounds)
    ciphertxt = s20.encrypt(message)
    #decrypt
    s20 = Salsa20(key, IV, rounds)
    plaintxt = s20.encrypt(ciphertxt)
    if message == plaintxt:
        print '    *** Exito ***'
    else:
        print '    *** Fallido ***'

if __name__ == '__main__':
    quick_test()
