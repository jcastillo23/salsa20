from Salsa20ImageEncryption import ImageEncryptSalsa
import threading
import wx
import wx.lib.newevent

#Creo eventos custom para disparar cuando terminan los threads de encriptacion y desencriptacion,
EncryptedEvent, EVT_ENCRYPTED_EVENT = wx.lib.newevent.NewEvent()
DecryptedEvent, EVT_DECRYPTED_EVENT = wx.lib.newevent.NewEvent()


class ImgViewer(wx.Frame):
    '''
    Clase que sirve para visualizar una imagen
    '''
    def __init__(self, parent, id, img_path):
        wx.Frame.__init__(self, parent, id, img_path, style=wx.DEFAULT_FRAME_STYLE ^ wx.RESIZE_BORDER)

        self.panel = wx.ScrolledWindow(self, wx.ID_ANY)
        self.panel.SetScrollbars(1, 1, 1, 1)

        image = wx.StaticBitmap(self.panel, wx.ID_ANY)
        im = wx.Bitmap(img_path)
        image.SetBitmap(im)
        image.SetToolTipString(img_path)

        sizer = wx.BoxSizer()
        sizer.Add(image)
        self.panel.SetSizer(sizer)

        x, y = im.GetSize()
        if x < 800 and y < 600:
            self.SetSize(im.GetSize())
        else:
            self.SetSize(wx.Size(800, 600))


class EncryptThread(threading.Thread):
    ''' Thread para encriptar o desencriptar la imagen y que no se haga en el main thread (GUI)'''
    def __init__(self, app, img, key, iv, mode='encrypt'):
        threading.Thread.__init__(self)
        self.app = app
        self.img = img
        self.key = key.decode('hex')
        self.iv = iv.decode('hex')
        self.mode = mode

    def run(self):
        if self.mode == 'encrypt':
            cifrador = ImageEncryptSalsa(self.img, self.key, self.iv, callback=self.app.progress_bar.on_timer)
            imgPath_encrypted = cifrador.encrypt()
            evt = EncryptedEvent(encrypted_img=imgPath_encrypted)
            wx.PostEvent(self.app, evt)
        elif self.mode == 'decrypt':
            cifrador = ImageEncryptSalsa(self.img,  self.key, self.iv, callback=self.app.progress_bar.on_timer)
            imgPath_decrypted = cifrador.decrypt()
            evt = DecryptedEvent(decrypted_img=imgPath_decrypted)
            wx.PostEvent(self.app, evt)


class ProgressBar(wx.Dialog):
    """
    Muestra una barra de progreso mientras se esta encriptando.
    """
    def __init__(self, gui, title):
        wx.Dialog.__init__(self, gui, title=title, style=wx.CAPTION)
        self.gui = gui
        self.count = 0
        self.gauge = wx.Gauge(self, range=100, size=(180, 30))
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.gauge, 0, wx.ALL, 10)

        self.SetSizer(sizer)
        sizer.Fit(self)
        self.SetFocus()

        # self.Bind(wx.EVT_TIMER, self.on_timer, self.timer)
        # self.timer.Start(30)

    def on_timer(self, to_add):
        self.count += to_add
        if self.count >= 100.0:
            self.count = 0
        self.gauge.SetValue(self.count)

class Salsa20GUI(wx.App):
    def __init__(self):
        wx.App.__init__(self)
        self.frame = wx.Frame(None, title='Salsa 20 Image Encryption', style=wx.DEFAULT_FRAME_STYLE ^ wx.RESIZE_BORDER)
        self.panel = wx.Panel(self.frame)
        self.IMGMAXSIZE = 100
        self.createWidgets()
        self.frame.Center()
        self.frame.Show()
        self.imgPath_plain = ""
        self.threads = []

    def createWidgets(self):
        '''Genera todos los objetos que van a estar en la interfaz grafica '''

        self.mainSizer = wx.BoxSizer(wx.VERTICAL)

        ######### TOP FRAME #########
        self.top_sizer = wx.BoxSizer(wx.VERTICAL)

        top_sizerUp = wx.BoxSizer(wx.HORIZONTAL)
        label_key = wx.StaticText(self.panel, -1, "Insert Key")
        top_sizerUp.Add(label_key, 0, wx.ALL, 5)
        self.textKey = wx.TextCtrl(self.panel, -1, "", size=(300, -1))
        self.textKey.SetToolTipString("Inserte una KEY de 16 o 32 bytes en hexadecimal (32|64 caracteres)")
        self.textKey.Bind(wx.EVT_CHAR, lambda event: self._restrict_input(event, 64))  # Maximo de clave 64 caracteres hexa (32 bytes)

        top_sizerUp.Add(self.textKey, 0, wx.ALL, 5)

        top_sizerDown = wx.BoxSizer(wx.HORIZONTAL)
        label_iv = wx.StaticText(self.panel, -1, "Insert IV")
        self.textIV = wx.TextCtrl(self.panel, -1, "", size=(300, -1))
        self.textIV.SetToolTipString("Inserte un IV de 8 bytes en hexadecimal (16 caracteres)")
        self.textIV.Bind(wx.EVT_CHAR, lambda event: self._restrict_input(event, 16))  # Maximo de IV 16 caracteres hexa (8 bytes)
        top_sizerDown.Add(label_iv, 0, wx.ALL, 5)
        top_sizerDown.Add(self.textIV, 0, wx.ALL, 5)

        self.top_sizer.Add(top_sizerUp, 0, wx.ALL, 5)
        self.top_sizer.Add(top_sizerDown, 0, wx.ALL, 5)
        ######### TOP FRAME #########

        ######### MIDDLE FRAME #########
        self.middle_sizer = wx.BoxSizer(wx.HORIZONTAL)

        img = wx.EmptyImage(self.IMGMAXSIZE, self.IMGMAXSIZE)
        self.imgPanel_plain = wx.StaticBitmap(self.panel, wx.ID_ANY, wx.BitmapFromImage(img))
        self.imgPanel_encrypted = wx.StaticBitmap(self.panel, wx.ID_ANY, wx.BitmapFromImage(img))
        self.imgPanel_decrypted = wx.StaticBitmap(self.panel, wx.ID_ANY, wx.BitmapFromImage(img))

        self.middle_sizer.Add(self.imgPanel_plain, 0, wx.ALL, 5)
        self.middle_sizer.Add(self.imgPanel_encrypted, 0, wx.ALL, 5)
        self.middle_sizer.Add(self.imgPanel_decrypted, 0, wx.ALL, 5)
        ######### MIDDLE FRAME #########

        ######### BOTTOM FRAME #########
        self.bottom_sizer = wx.BoxSizer(wx.HORIZONTAL)
        btn_open = wx.Button(self.panel, label='Open')
        btn_open.Bind(wx.EVT_BUTTON, self._on_open)

        self.btn_encrypt = wx.Button(self.panel, label='Encrypt')
        self.btn_encrypt.Bind(wx.EVT_BUTTON, self._on_encrypt)
        self.btn_encrypt.Disable()

        self.btn_decrypt = wx.Button(self.panel, label='Decrypt')
        self.btn_decrypt.Bind(wx.EVT_BUTTON, self._on_decrypt)
        self.btn_decrypt.Disable()

        self.bottom_sizer.Add(btn_open, 0, wx.ALL, 5)
        self.bottom_sizer.Add(self.btn_encrypt, 0, wx.ALL, 5)
        self.bottom_sizer.Add(self.btn_decrypt, 0, wx.ALL, 5)
        ######### BOTTOM FRAME #########

        self.mainSizer.Add(self.top_sizer, 0, wx.ALL, 5)
        self.mainSizer.Add(self.middle_sizer, 0, wx.ALL, 5)
        self.mainSizer.Add(self.bottom_sizer, 0, wx.ALL, 5)

        self.panel.SetSizer(self.mainSizer)
        self.mainSizer.Fit(self.frame)
        self.panel.Layout()

    def _clean_up_miniatures(self):
        ''' Borra las miniaturas '''
        img = wx.BitmapFromImage(wx.EmptyImage(self.IMGMAXSIZE, self.IMGMAXSIZE))
        self.imgPanel_plain.SetBitmap(img)
        self.imgPanel_encrypted.SetBitmap(img)
        self.imgPanel_decrypted.SetBitmap(img)

    def _show_image(self, event, img_path):
        ''' Muestra la imagen al hacer click en la miniatura'''
        imgViewer = ImgViewer(None, wx.ID_ANY, img_path)
        imgViewer.Show()

    def _show_alert(self, msg):
        ''' Muestra un mensaje de alerta '''
        dlg = wx.MessageDialog(self.frame, msg, 'ALERT',
                               wx.OK | wx.ICON_INFORMATION)
        dlg.ShowModal()
        dlg.Destroy()

    def _validate_inputs(self):
        ''' Valida los inputos que ingresa el usuario'''
        key, iv = self.textKey.GetValue(), self.textIV.GetValue()
        if not key or len(key) not in [32, 64]:
            self._show_alert("Debe Proveer una KEY de 16 o 32 bytes(32 o 64 caracteres hexa) de longitud")
            return None
        if not iv or len(iv) != 16:
            self._show_alert("Debe Proveer un Vector de Inicializacion de 8 bytes(16 caracters hexa) de longitud")
            return None

        return key, iv

    def _restrict_input(self, event, maxLen):
        '''
        Nos permite solo ingresar keys e IV que sean caracteres hexa.
        '''

        key = event.GetKeyCode()
        if key == wx.WXK_DELETE or key == wx.WXK_BACK or key > 255:
            event.Skip()
            return
        text_ctrl = event.GetEventObject()
        if len(text_ctrl.GetValue()) == maxLen:
            return False
        acceptable_characters = "0123456789ABCDEFabcdef"

        if chr(key) in acceptable_characters:
            text_ctrl.AppendText(chr(key).upper())
            return
        else:
            return False

    # def on_progress_bar_increment(self, event, percent):
    #     self.progress_bar.

    def on_encrypt_finish(self, event):
        '''Evento a dispararse al terminar de encriptar la imagen
           recibe el path de la imagen descenriptada y lo muestra en la miniatura.
        '''
        for t in self.threads:
            t.join()
        self.threads = list()
        self.progress_bar.Destroy()
        self.btn_encrypt.Disable()
        self.imgPath_encrypted = event.encrypted_img
        self._show_minature(self.imgPanel_encrypted, self.imgPath_encrypted)

    def _on_encrypt(self, event):
        ''' Encripta la imagen y la muestra en la miniatura'''
        inputs = self._validate_inputs()
        if not inputs:
            return
        KEY, IV = inputs
        self.progress_bar = ProgressBar(self.frame, "Encriptando....")

        thread1 = EncryptThread(self, self.imgPath_plain, KEY, IV, mode='encrypt')
        self.threads.append(thread1)
        self.progress_bar.Show()
        thread1.start()
        self.Bind(EVT_ENCRYPTED_EVENT, self.on_encrypt_finish)

        # self.KEY, self.IV = inputs
        # cifrador = ImageEncryptSalsa(self.imgPath_plain, self.KEY.decode('hex'), self.IV.decode('hex'))
        # self.imgPath_encrypted = cifrador.encrypt()
        # self._show_minature(self.imgPanel_encrypted, self.imgPath_encrypted)
        # self.btn_encrypt.Disable()

    def on_decrypt_finish(self, event):
        '''Evento a dispararse al terminar de desencriptar la imagen
           recibe el path de la imagen descenriptada y lo muestra en la miniatura.
        '''
        for t in self.threads:
            t.join()
        self.progress_bar.Destroy()
        self.imgPath_decrypted = event.decrypted_img
        self._show_minature(self.imgPanel_decrypted, self.imgPath_decrypted)
        self.threads = list()
        self.btn_decrypt.Disable()

    def _on_decrypt(self, event):
        ''' Descencripta la imagen y la muestra en la miniatura'''
        inputs = self._validate_inputs()
        if not inputs:
            return
        KEY, IV = inputs
        self.progress_bar = ProgressBar(self.frame, "Desencriptando....")
        thread2 = EncryptThread(self, self.imgPath_encrypted, KEY, IV, mode='decrypt')
        self.threads.append(thread2)
        self.progress_bar.Show()
        thread2.start()
        self.Bind(EVT_DECRYPTED_EVENT, self.on_decrypt_finish)

        # cifrador = ImageEncryptSalsa(self.imgPath_encrypted, self.KEY.decode('hex'), self.IV.decode('hex'))
        # self.imgPath_decrypted = cifrador.decrypt()
        # self._show_minature(self.imgPanel_decrypted, self.imgPath_decrypted)
        # self.btn_decrypt.Disable()

    def _on_open(self, event):
        '''Abre el archivo'''
        self._clean_up_miniatures()
        wildcard = "PNG files (*.png)|*.png|BMP files (*.bmp)|*.bmp|JPEG files (*.jpg)|*.jpg"
        dialog = wx.FileDialog(None, "Choose a file", defaultDir=".", style=wx.OPEN, wildcard=wildcard)
        if dialog.ShowModal() == wx.ID_OK:
            self.imgPath_plain = dialog.GetPath()
            dialog.Destroy()
        else:
            dialog.Destroy()
            return

        self.imgPath_encrypted = ""
        self.imgPath_decrypted = ""
        self.btn_decrypt.Enable()
        self.btn_encrypt.Enable()
        self._show_minature(self.imgPanel_plain, self.imgPath_plain)

    def _show_minature(self, img_panel, img_path):
        '''Genera la minatura de la imagen '''
        img = wx.Image(img_path, wx.BITMAP_TYPE_ANY)
        #Escalar la miniatura de la imagen
        W = img.GetWidth()
        H = img.GetHeight()
        if W > H:
            newW = self.IMGMAXSIZE
            newH = self.IMGMAXSIZE * H / W
        else:
            newH = self.IMGMAXSIZE
            newW = self.IMGMAXSIZE * W / H
        img = img.Scale(newW, newH)

        img_panel.SetBitmap(wx.BitmapFromImage(img))
        img_panel.Bind(wx.EVT_LEFT_DOWN, lambda event: self._show_image(event, img_path))
        self.panel.Refresh()

    def Close(self):
        wx.App.Close(self)

if __name__ == '__main__':
    app = Salsa20GUI()
    app.MainLoop()
